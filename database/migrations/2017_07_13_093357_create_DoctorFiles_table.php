<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DoctorFiles', function (Blueprint $table) {
           $table->increments('id');
            $table->text('name');
            $table->text('description');
            $table->integer('doctor_level_id')->unsigned();
            $table->boolean('is_active');
            $table->timestamps();
            $table->foreign('doctor_level_id')->references('id')->on('DoctorLevels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DoctorFiles');
    }
}
