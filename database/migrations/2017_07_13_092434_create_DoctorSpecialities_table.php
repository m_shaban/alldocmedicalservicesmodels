<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DoctorSpecialities', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title_en');
            $table->text('title_ar');
            $table->Integer('parent_id')->references('id')->on('DoctorSpecialities');
          //  $table->foreign('parent_id')->references('id')->on('DoctorSpecialities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DoctorSpecialities');
    }
}
