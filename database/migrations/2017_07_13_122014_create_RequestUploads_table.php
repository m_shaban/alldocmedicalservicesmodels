<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RequestUploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('upload');
            $table->integer('request_header_id')->unsigned();
            $table->foreign('request_header_id')->references('id')->on('RequestHeader');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RequestUploads');
    }
}
