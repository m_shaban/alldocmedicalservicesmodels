<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DoctorUploads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_files_id')->unsigned();
            $table->integer('doctor_id')->unsigned();
            $table->text('file');
            $table->boolean('is_approved');
            
            $table->foreign('doctor_files_id')->references('id')->on('DoctorFiles');
           $table->foreign('doctor_id')->references('id')->on('Doctors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DoctorUploads');
    }
}
