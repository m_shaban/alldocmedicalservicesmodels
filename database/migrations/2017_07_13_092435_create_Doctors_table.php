<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Doctors', function (Blueprint $table) {
            $table->increments('id');
            
             $table->text('gender');
            $table->text('current_credit');
            $table->text('user_state');
            $table->text('graduation_year');
            $table->text('short_bio_arabic');
            $table->text('short_bio_english');
            $table->integer('country_id')->unsigned();
            $table->string('neighborhood',255);
            $table->integer('doctor_level_id')->unsigned();
            $table->integer('doctor_speciality_id')->unsigned();
            $table->string('referral_number',255);
            $table->string('practicing',255);
            $table->string('doctor_identification_number',255);
            $table->string('profile_img',255);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
           $table->foreign('user_id')->references('id')->on('Users');
            $table->foreign('doctor_level_id')->references('id')->on('DoctorLevels');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('doctor_speciality_id')->references('id')->on('DoctorSpecialities');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Doctors');
    }
}
