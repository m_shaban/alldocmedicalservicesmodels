<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DoctorLevels', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title_en');
            $table->text('title_ar');
            $table->decimal('offline_question');
            $table->decimal('chat');
            $table->decimal('call');
            $table->decimal('visit');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DoctorLevels');
    }
}
