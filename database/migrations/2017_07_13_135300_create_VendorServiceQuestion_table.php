<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorServiceQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('VendorServiceQuestion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('vendor_service_id')->unsigned(); //vendorservices
            $table->integer('question_type_id')->unsigned(); //vendorservices
            $table->timestamps();
            $table->foreign('vendor_service_id')->references('id')->on('VendorServices');
            $table->foreign('question_type_id')->references('id')->on('QuestionTypes');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('VendorServiceQuestion');
    }
}
