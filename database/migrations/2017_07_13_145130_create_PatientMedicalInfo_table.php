<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientMedicalInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PatientMedicalInfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('medical_answer');
            $table->integer('patient_id')->unsigned();
            $table->integer('medical_info_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('patient_id')->references('id')->on('Patient');
            $table->foreign('medical_info_id')->references('id')->on('MedicalInfo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PatientMedicalInfo');
    }
}
