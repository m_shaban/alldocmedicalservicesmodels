<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddPatientRelativeKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('PatientRelatives', function (Blueprint $table) {
            $table->integer('relative_id')->unsigned();
            $table->foreign('relative_id')->references('id')->on('Relatives');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('PatientRelatives', function (Blueprint $table) {
            $table->dropForeign(['relative_id']);
           
        });
    }
}
