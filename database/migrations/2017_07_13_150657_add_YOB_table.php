<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYOBTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('Patient', function (Blueprint $table) {
            $table->integer('year_of_brith');
            
        });
        Schema::table('DoctorPatients', function (Blueprint $table) {
            $table->integer('age');
            
        });
        Schema::table('PatientRelatives', function (Blueprint $table) {
            $table->integer('year_of_brith');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
