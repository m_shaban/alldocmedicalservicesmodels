<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalInfo extends Model
{
    protected $table = 'MedicalInfo';
     public function PatientMedicalInfo(){
        return $this->hasMany(PatientMedicalInfo::class,'medical_info_id','id');
        }
}
