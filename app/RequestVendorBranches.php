<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestVendorBranches extends Model
{
    protected $table = 'RequestVendorBranches';
    public function RequestDetail(){
        return $this->belongsTo(RequestDetail::class,'request_details_id','id');
        }
         public function VendorItemBranches(){
        return $this->belongsTo(VendorItemBranches::class,'vendor_item_branches_id','id');
        }
}
