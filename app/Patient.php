<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    public function User(){
        return $this->belongsTo(User::class,'user_id','id');
        }
    public function patientRelatives(){
        return $this->hasMany(patientRelatives::class,'patient_id','id');
        }
         public function PatientMedicalInfo(){
        return $this->hasMany(PatientMedicalInfo::class,'patient_id','id');
        }
}
