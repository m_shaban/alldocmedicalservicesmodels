<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorFiles extends Model
{
    protected $table = 'DoctorFiles';
    public function DoctorLevels(){
        return $this->belongsTo(DoctorLevels::class,'doctor_level_id','id');
        }
        public function DoctorUploads(){
        return $this->hasMany(DoctorUploads::class,'doctor_files_id','id');
        }
}
