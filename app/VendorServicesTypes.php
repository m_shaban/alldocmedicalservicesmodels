<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorServicesTypes extends Model
{
    protected $table = 'VendorServicesTypes';
    public function VendorServices(){
        return $this->belongsTo(VendorServices::class,'vendor_service_id','id');
        }
        
        
         public function VendorItems(){
        return $this->hasMany(VendorItems::class,'vendor_service_type_id','id');
        }
}
