<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestQuestion extends Model
{
    protected  $table = 'RequestQuestion';
    
    public function RequestHeader(){
        return $this->belongsTo(RequestHeader::class,'request_id','id');
        }
        
        public function VendorServiceQuestion(){
        return $this->belongsTo(VendorServiceQuestion::class,'vendor_service_question_id','id');
        }
}
