<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    protected $table = 'Doctors';
   
        public function DoctorSpecialities(){
        return $this->belongsTo(DoctorSpecialities::class,'doctor_speciality_id','id');
        }
        public function DoctorLevels(){
        return $this->belongsTo(DoctorLevels::class,'doctor_level_id','id');
        }
        public function Countries(){
        return $this->belongsTo(Countries::class,'country_id','id');
        }
        
        public function DoctorUploads(){
        return $this->hasMany(DoctorUploads::class,'doctor_id','id');
        }
        public function DoctorPatients(){
        return $this->hasMany(DoctorPatients::class,'doctor_id','id');
        } 
        
}
