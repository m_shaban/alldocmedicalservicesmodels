<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorPatients extends Model
{
    protected $table = 'DoctorPatients';
     public function Doctors(){
        return $this->belongsTo(Doctors::class,'doctor_id','id');
        }   
}
