<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorItems extends Model
{
    protected $table = 'VendorItems';
    public function VendorServicesTypes(){
        return $this->belongsTo(VendorServicesTypes::class,'vendor_service_type_id','id');
        }
        
    public function VendorItemBranches(){
        return $this->hasMany(VendorItemBranches::class,'vendor_item_id','id');
        }    
         public function RequestDetail(){
        return $this->hasMany(RequestDetail::class,'vendor_item_id','id');
        }
        
}
