<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestUploads extends Model
{
    protected $table = 'RequestUploads';
     public function RequestHeader(){
        return $this->belongsTo(RequestHeader::class,'request_header_id','id');
     }
}
