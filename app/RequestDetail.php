<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDetail extends Model
{
    protected $table = 'RequestDetail';
     public function RequestHeader(){
        return $this->belongsTo(RequestHeader::class,'request_header_id','id');
     }
        public function VendorItems(){
        return $this->belongsTo(VendorItems::class,'vendor_item_id','id');
        }
        public function RequestVendorBranches(){
        return $this->hasMany(RequestVendorBranches::class,'request_details_id','id');
        }
     
}
