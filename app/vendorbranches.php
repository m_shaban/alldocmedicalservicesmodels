<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vendorbranches extends Model
{
    
        protected $table = 'VendorBranches';
    

    public function vendor(){
        return $this->belongsTo(vendor::class,'vendor_id','id');
    }
    public function countries(){
        return $this->belongsTo(countries::class,'country_id','id');
    }
    public function vendorcntacts(){
        return $this->hasMany(vendorcntacts::class,'vendor_branch_id','id');
    }
     public function VendorItemBranches(){
        return $this->hasMany(VendorItemBranches::class,'branch_id','id');
        }    
}
