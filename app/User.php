<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username' ,'email', 'password','user_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user_type(){
        return $this->belongsTo(UserType::class,'user_type_id','id');
    }
    public function Patient(){
        return $this->hasMany(Patient::class,'user_id','id');
        }
        public function RequestHeader(){
        return $this->hasMany(RequestHeader::class,'user_id','id');
        }
}
