<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorSpecialities extends Model
{
    protected $table = 'DoctorSpecialities';
    
    public function DoctorSpecialities(){
        return $this->hasOne(DoctorSpecialities::class,'parent_id','id');
        }
         public function Doctors(){
        return $this->hasMany(Doctors::class,'doctor_speciality_id','id');
        }
}
