<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'countries';
    public function Doctors(){
        return $this->hasMany(Doctors::class,'country_id','id');
        }
         public function vendorbranches(){
        return $this->hasMany(vendorbranches::class,'country_id','id');
    }
}
