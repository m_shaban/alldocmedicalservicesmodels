<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class request_status extends Model
{
    protected  $table = 'RequestStatus';
    public function RequestHeader(){
        return $this->hasMany(RequestHeader::class,'request_status_id','id');
        }
        public function RequestLog(){
        return $this->hasMany(RequestLog::class,'request_status_id','id');
        }
}
