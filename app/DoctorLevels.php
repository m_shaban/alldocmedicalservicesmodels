<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorLevels extends Model
{
    protected $table = 'DoctorLevels';
    public function Doctors(){
        return $this->hasMany(Doctors::class,'doctor_level_id','id');
        }
        public function DoctorFiles(){
        return $this->belongsTo(DoctorFiles::class,'doctor_level_id','id');
        }
}
