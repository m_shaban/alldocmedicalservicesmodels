<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientMedicalInfo extends Model
{
    protected $table = 'PatientMedicalInfo';
    public function Patient(){
        return $this->belongsTo(Patient::class,'patient_id','id');
        }
        
        public function MedicalInfo(){
        return $this->belongsTo(MedicalInfo::class,'medical_info_id','id');
        }
}
