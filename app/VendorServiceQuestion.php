<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestQuestionVendorServiceQuestion extends Model
{
    
    
    protected  $table = 'VendorServiceQuestion';
    public function VendorServices(){
        return $this->belongsTo(VendorServices::class,'vendor_service_id','id');
        }
        public function QuestionTypes(){
        return $this->belongsTo(QuestionTypes::class,'question_type_id','id');
        }
        public function RequestQuestion(){
        return $this->hasMany(RequestQuestion::class,'vendor_service_question_id','id');
        }
}
