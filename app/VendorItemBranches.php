<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorItemBranches extends Model
{
    protected $table = 'VendorItemBranches';
    public function VendorItems(){
        return $this->belongsTo(VendorItems::class,'vendor_item_id','id');
        }
        public function VendorBranches(){
        return $this->belongsTo(VendorBranches::class,'branch_id','id');
        }
        public function RequestVendorBranches(){
        return $this->hasMany(RequestVendorBranches::class,'vendor_item_branches_id','id');
        }
}
