<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorUploads extends Model
{
    protected $table = 'DoctorUploads';
    public function DoctorFiles(){
        return $this->belongsTo(DoctorFiles::class,'doctor_files_id','id');
        }
        public function Doctors(){
        return $this->belongsTo(Doctors::class,'doctor_id','id');
        }
}
