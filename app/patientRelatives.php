<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class patientRelatives extends Model
{
    protected $table = 'patientRelatives';
     public function Patient(){
        return $this->belongsTo(Patient::class,'patient_id','id');
        }
        public function Relatives(){
        return $this->belongsTo(Relatives::class,'relative_id','id');
        }
}
