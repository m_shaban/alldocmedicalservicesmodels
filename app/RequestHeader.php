<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestHeader extends Model
{
    protected $table = 'RequestHeader';
     public function User(){
        return $this->belongsTo(User::class,'requester_id','id');
        }
        public function RequestDetail(){
        return $this->hasMany(RequestDetail::class,'request_header_id','id');
     }
     public function RequestUploads(){
        return $this->hasMany(RequestUploads::class,'request_header_id','id');
     }
     
      public function RequestQuestion(){
        return $this->hasMany(RequestQuestion::class,'request_id','id');
        }
        public function request_status(){
        return $this->belongsTo(request_status::class,'request_status_id','id');
        }
        public function RequestLog(){
        return $this->hasMany(RequestLog::class,'request_header_id','id');
        }
}
