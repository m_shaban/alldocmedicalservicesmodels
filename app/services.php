<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class services extends Model
{
    
        protected $table = 'Services';
    
       public function servicetypes(){
        return $this->belongsTo(servicetypes::class,'service_type_id','id');
    } 
   public function servicefiles(){
        return $this->hasMany(servicefiles::class,'service_id','id');
    }
    public function vendorservices(){
        return $this->hasMany(vendorservices::class,'service_id','id');
    }
   
}
