<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionTypes extends Model
{
    protected $table ='QuestionTypes';
     public function VendorServiceQuestion(){
        return $this->hasMany(VendorServiceQuestion::class,'question_type_id','id');
        }
}
